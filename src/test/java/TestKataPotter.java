import org.junit.Test;
import org.shiroling.KataPotter;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestKataPotter {

    @Test
    public void test1Livre() {
        KataPotter kata = new KataPotter();
        assertEquals(8, kata.compute(List.of(1,0,0,0,0)),  0.01);
    }

    @Test
    public void test2Livres() {
        KataPotter kata = new KataPotter();
        assertEquals(15.2, kata.compute(List.of(1,1,0,0,0)),  0.01);
    }

    @Test
    public void test3Livres() {
        KataPotter kata = new KataPotter();
        assertEquals(21.6, kata.compute(List.of(1,1,1,0,0)), 0.01);
    }

    @Test
    public void test4Livres() {
        KataPotter kata = new KataPotter();
        assertEquals(25.6, kata.compute(List.of(1,1,1,1,0)), 0.01);
    }

    @Test
    public void test5Livres() {
        KataPotter kata = new KataPotter();
        assertEquals(30, kata.compute(List.of(1,1,1,1,1)), 0.01);
    }
    @Test
    public void test2livresSimilaires() {
        KataPotter kata = new KataPotter();
        assertEquals(16,kata.compute(List.of(2,0,0,0,0)),  0.01);
    }
    
    @Test
    public void testExempleSujet() {
        KataPotter kata = new KataPotter();
        assertEquals(51.2, kata.compute(List.of(2,2,2,1,1)),  0.01);
    }

    @Test
    public void testProposeParUnPairSusnommeNoa() {
        KataPotter kata = new KataPotter();
        assertEquals(108, kata.compute(List.of(1,2,3,4,5)),  0.01);
    }

    @Test
    public void testProposeParMrLeblanc() {
        KataPotter kata = new KataPotter();
        assertEquals(124, kata.compute(List.of(5,5,5,2,2)),  0.01);
    }


}
