package org.shiroling;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class KataPotter {
    private static float[] prix = {8f, 15.2f, 21.6f, 25.6f, 30f};


    public double compute(List<Integer> commande) {
        List<Integer> list = new ArrayList<>();
        for (int l : commande) {
            list.add(l);
        }

        int nbLivres = nbLivres(commande);
        if (nbLivres == nbLivresDifferents(commande)) {
            return prix[nbLivres-1];
        }

        double result = 0;
        for (int i = 0; i < nbLots(commande); i++) {
            result += fairePaquet(list);
        }

        return result;
    }

    private double fairePaquet(List<Integer> commande) {
        int nbLivreCherche = nbLivresParLot(commande);
        Collections.sort(commande);
        Collections.reverse(commande);
        int i = 0;
        int count = 0;
        for (int nl : commande) {
            if (nl > 0) {
                count ++;
                commande.set(i, nl-1);
                if (count == nbLivreCherche){
                    break;
                }
            }
            i++;
        }
        return prix[count-1];
    }

    private int nbLivresDifferents(List<Integer> commande) {
        int count = 0;
        for (int i : commande) {
            if (i > 0)
                count ++;
        }
        return count;
    }

    private int nbLivres(List<Integer> commande) {
        int count = 0;
        for (int i : commande) {
            count += i;
        }
        return count;
    }

    private int nbLots(List<Integer> commande) {
        int max = 0;
        for (int i : commande) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    private int nbLivresParLot(List<Integer> commande){
        return nbLivres(commande)/nbLots(commande);
    }
}
